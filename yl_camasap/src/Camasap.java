import java.awt.Dimension;
import java.awt.image.BufferedImage;

import webcam.WebcamController;

public class Camasap {

  public static void main(String[] args) throws InterruptedException {
    WebcamController wc = new WebcamController(new Dimension(640, 480));
    wc.initFrame( true );
    BufferedImage image = wc.takePicture();
    wc.savePicture(image, "JPEG", "../pictures/image.jpg");
  }
	

}

